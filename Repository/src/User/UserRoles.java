package User;

import java.util.ArrayList;
import java.util.List;

public class UserRoles extends Entity {
	
	private int userId;
	private int roleId;	
	private List<RolesPermission> rolesPermission;
	private List<User> users;
	
	public UserRoles() {
		
		setRolesPermission(new ArrayList<RolesPermission>());
		setUsers(new ArrayList<User>());
		
	}
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	public int getRoleId() {
		return roleId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public List<RolesPermission> getRolesPermission() {
		return rolesPermission;
	}

	public void setRolesPermission(List<RolesPermission> rolesPermission) {
		this.rolesPermission = rolesPermission;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
	
	public void addUser(User user) {
		users.add(user);
	}
	
	public void addRolePermission(RolesPermission rolesPermission) {
		this.rolesPermission.add(rolesPermission);
	}

}
