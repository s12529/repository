package User;

import java.util.ArrayList;
import java.util.List;

public class User extends Entity {
	
	private String login;
	private String password;
	private List<UserRoles> userRoles;
	
	public User() {
			setUserRoles(new ArrayList<UserRoles>());
	}
		
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public List<UserRoles> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(List<UserRoles> userRoles) {
		this.userRoles = userRoles;
	}
	
	public void addRole(UserRoles role) {
		userRoles.add(role);
	}

	
}
