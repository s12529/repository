package UoW;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import User.Entity;
import User.EntityState;

public class HsqlUnitOfWork implements UnitOfWork {

	private Connection connection;
	private Map<Entity,UnitOfWorkRepository> entities = new LinkedHashMap<Entity, UnitOfWorkRepository>();
	
	public HsqlUnitOfWork(Connection connection) {
		this.connection = connection;
		
		try {
			connection.setAutoCommit(false);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void saveChanges() {
		for(Entity entity: entities.keySet())
		{
			switch(entity.getState())
			{
			case NEW:
				entities.get(entity).persistAdd(entity);
				break;
			case MODIFIED:
				entities.get(entity).persistUpdate(entity);
				break;
			case DELETED:
				entities.get(entity).persistRemove(entity);
				break;
			case UNCHANGED:
				break;
			case UNKNOWN:
				break;
			default:
				break;}
		}
		
		try {
			connection.commit();
			entities.clear();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void undo() {
		entities.clear();
	}

	@Override
	public void markAsNew(Entity entity, UnitOfWorkRepository repo) {
		entity.setState(EntityState.NEW);
		entities.put(entity, repo);
	}

	@Override
	public void markAsDeleted(Entity entity, UnitOfWorkRepository repo) {
		entity.setState(EntityState.DELETED);
		entities.put(entity, repo);
	}

	@Override
	public void markAsChanged(Entity entity, UnitOfWorkRepository repo) {
		entity.setState(EntityState.MODIFIED);
		entities.put(entity, repo);
	}

	public Map<Entity,UnitOfWorkRepository> getEntities() {
		return entities;
	}

	public void setEntities(Map<Entity,UnitOfWorkRepository> entities) {
		this.entities = entities;
	}
	

}
