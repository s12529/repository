package Repository;

import java.util.ArrayList;
import java.util.List;

import User.EnumerationValue;
import User.RolesPermission;
import User.User;
import User.UserRoles;

public class Database {

	List<User> users;
	List<RolesPermission> userPermission;
	List<UserRoles> userRoles;
	List<EnumerationValue> enumValues;
	
	
	public Database() {
		
		users = new ArrayList<User>();
		userPermission = new ArrayList<RolesPermission>();
		userRoles = new ArrayList<UserRoles>();
		enumValues = new ArrayList<EnumerationValue>();
		
	}
	
	
}
