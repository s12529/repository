package Repository;

import User.EnumerationValue;
import User.User;

public interface RepositoryCatalog {
	
	public Repository<EnumerationValue> enumerations();
	public Repository<User> users();
	

}
