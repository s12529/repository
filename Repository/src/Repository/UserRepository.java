package Repository;
import User.User;

public interface UserRepository extends Repository<User>{
	
	public User withLogin(String login);
	public User withLoginAndPassword(String login, String password);
	void setupPermission(User user);

}
